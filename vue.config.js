var path = require("path");

module.exports = {
  devServer: {
    proxy: "http://localhost:5000",
  },
  transpileDependencies: ["vuetify"],
  configureWebpack: {
    resolve: {
      alias: {
        common: path.resolve(__dirname, "common"),
      },
    },
  },
  publicPath: process.env.BASE_URL
};
