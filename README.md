# icms-tools-front

## About the project
The front-end single page application (SPA) of icms-tools, a project for various, smaller CMS-related utilities. Built with [Vue](https://vuejs.org/) and [Vuetify](https://vuetifyjs.com/en/)

## Getting started

### Prerequisites
This project requires [Node.js](https://nodejs.org/en/) v14.0+ and [npm](https://www.npmjs.com/get-npm) v6.14+ to be installed locally

### Installation
To get the project up and running, and view components in the browser, complete the following steps:

1. Download and install [Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/get-npm)
2. Clone this repo ( IMPORTANT: Use the `--recurse-submodules` flag ): `git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/cms-icmsweb/icms-tools-front.git` (SSH)
3. Copy the contents of the `.env.local` file. Create a `.env` file at the project's root directory and paste.
4. Install project dependencies: `npm install`
5. Start the development environment: `npm run serve`
6. Open your browser and visit <http://localhost:8080>

### Development
When developing components, you may want assets automatically compiled and the browser to refresh automatically. To do this, run the following task:
* `npm run dev`

### Create a static build
To create production ready static instance of this project run:
* npm run build -- --mode= < environment >
This will create a `dist` directory into which the required files will be created.
The --mode parameter will determine which .env.<environment> file will get included in the final build.

## Development Tools
### Linting
[ESLint](https://eslint.org/) is used for linting. The project-specific configuration rules are contained in the `.eslintrc.js` file. 
### Formating
All of the project's code is automatically formatted by [Prettier](https://prettier.io/).

### Setup for Visual Studio Code
To integrate the tools into VS Code for automatic linting and formatting:
* Install the [ESLint for VS Code Extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
* Install the [Prettier VS Code Extension](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
* Create a `.vscode` directory. Inside the directory create a `settings.json` file with the following contents: 
```
   {
    "eslint.options": { "configFile": "./.eslintrc.js" },  

    "editor.formatOnSave": false,
    "[javascript]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode",
        "editor.formatOnSave": true,
        "editor.tabSize": 2,
    },
    "[vue]": {
        "editor.formatOnSave": true,
        "editor.defaultFormatter": "esbenp.prettier-vscode",
        "editor.tabSize": 2,
    },
}
```


## Project overview / Style guide
### Folder structure
```
.
├── src/
    ├── api/
        ├── core/
        ├── endpoints/
        ├── index.js
    ├── assets/
    ├── components/
        ├── common/
        ├── layout/
        └── pages/
    ├── mixins/
    ├── plugins/
    ├── router/
        ├── routes/
        └── index.js
    ├── store/
        ├── modules/
        └── index.js
    ├── utils/
    ├── App.vue
    └── main.js
```
### Components 
#### Folder structure
The `/components` folder currently separates the components into 3 different categories with their respective subfolders:
* `/common` for components that are being used by multiple other components
* `/layout` for components that are not part of the router-view (`AppBar`, `AppFooter`, etc...)
* `/pages` for components that act as views for routing and their subcomponents. Due to the large number of page-components they are divided into subfolders based on their grouping in the menu (`/collaboration`, `/institute`, etc ...)

The structure loosely follows for the [Component Folder Pattern](https://vueschool.io/articles/vuejs-tutorials/structuring-vue-components/).

#### Naming
Avoid using one-word component names like `Button.vue` or `Footer.vue` as specified by [vue's style guide](https://vuejs.org/v2/style-guide/#Multi-word-component-names-essential).
#### Global Components
Components that are frequently used across the project are registered globally in the `main.js` file. Global components do not require importing and can be used directly in the DOM.
Example: 
```
Vue.component("base-page", BasePage);
Vue.component("data-table", DataTable);
```

### Global state management
The application's global state is managed by a [vuex store](https://vuex.vuejs.org/)
#### Modules
The store is divided into [modules](https://vuex.vuejs.org/guide/modules.html). all modules located in the `@/store/modules` directory with each file representing different module. All modules are namespaced thus accessing their properties directly requires using the `<moduleName>/<propertyName>` syntax (Example: `store.dispatch('notification/infoMessage', message);`).
#### Accessing the store
Inside components avoid accessing the store directly using `this.$store`. Instead it is preferable to use one of the `{ mapState, mapGetters, mapActions }` [helpers](https://vuex.vuejs.org/api/#component-binding-helpers) provided by `vuex`.

### API 
The project uses [axios](https://github.com/axios/axios) as the HTTP client.
All of the api related logic leaves inside the `@/api` folder.
* The `./core` folder exports all the utilities used to create each endpoint class. 
    * The `restplusApi` object inside `@/api/core/axiosWrapper` is a wrapper on `axios` to configure an axios instance for the restplus api.
    * The `BaseCall` class inside `@/api/core/BaseCalls` contains the functionality to define a specific endpoint and its parameters. Any class instance that extends `BaseCall` can perform an api request by calling `get`, `post`, `put` or `delete`. Those methods return a promise that either resolves to the call's response or fails with an error.
    * the module exports classes extending `BaseCall` used for creating the various different endpoint classes for each specific endpoint . Those are `RestplusCall` and also `PiggyBackCall` as well as other utilities like `Hateos` and `IdResolvers` 
* The `./endpoints` folder contains the endpoint-specific classes 
    * Those classes are divided into different files based on the endpoint's namespace ( `icms_public`, `members`, `authorship`, etc... )   
    * Finally they are exported by `@/api` to make accessing them easier

An example of performing an api call:
Create the endpoint's class by extending the `RestplusApi` call.
```
export class AuthorizationsCall extends RestplusCall {
  constructor() {
    super("/admin/permissions/rights");
  }
  setResourceId(value) {
    return this.set("resource_id", value);
  }
  setAccessClassId(value) {
    return this.set("access_class_id", value);
  }
  setAction(value) {
    return this.set("action", value);
  }
}
```
Inside a components method create an instance of the class and perform the api call.
```
<script>
import api from "@/api";
export default {
    ...
    methods: {
        ...
        async loadAuths(){
            this.auths = await api.AuthorizationsCall().get();
        }
        async storeAuth(){
            const newAuth = await api.AuthorizationsCall()
                .setAction(this.editors.permissions.action)
                .setAccessClassId(this.editors.permissions.access_class_id)
                .setResourceId(this.editors.permissions.resource_id)
                .post();
            ...
        }
    }
}
</script>
```
**NOTE** : If no method of catching the error thrown from a failed api call is provided ( either by a `try catch` block `catch(error => {...})` or `setFailureCallback` ) the application will throw an errorNotification by default.

### Importing / Exporting
To avoid long relative paths when importing modules the webpack alias `@` = `./src` is used.
For example:
```
import { rule } from '../../../validation'
```
Can be written as:  
```
import { rule } from '@/utils/validation'
```
Vue-CLI's Webpack configuration automatically resolves the `.js` and `.vue` file extensions and automatically imports from `./index.js` and `./index.vue` when the path ends with a directory.
For example:
```
import { icon } from '@/utils/constants.js';
import api from '@/api/index.js';
import ComponentA from '@/components/ComponentA.vue'
import ComponentB from '@/components/ComponentB/index.vue'; 
```
Can be written as:
```
import { icon } from '@/utils/constants';
import api from '@/api';
import ComponentA from '@/components/ComponentA'
import ComponentB from '@/components/ComponentB'; 
```


### Menu
The app-bar's menu configuration is contained in the utils `@/utils/menu` file.
After creating a route for a new page.
Each menu object represents either:
* A menu item `{ title: <Page Name>, route: <Route to the Page> }`
* Or a submenu `{ title: <Submenu name>, items: <Array of menu items> }`

### Icons
The project uses the items provided by [material design](https://materialdesignicons.com/).
To organize the abbreviations used by material design icons an `icon` object is exported by `@/utils/constants`. When adding using new icons to the project it is preferred to include them in the icons object first (Ex. `{ ... search: 'mdi-magnify' }`).
To be able use a constant icon value inside a component's template add them to the vue instance in the `created` method:
```
<script>
import { icon } from '@/utils/constants'
export default {
    ...
    created(){
        this.icon = icon
    }
}
</script>
```


## Testing
TODO

