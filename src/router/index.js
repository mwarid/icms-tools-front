import Vue from "vue";
import Router from "vue-router";

import collaboration from "./routes/collaboration";
import institute from "./routes/institute";
import administration from "./routes/administration";
import publications from "./routes/publications";
import user from "./routes/user";
import base from "./routes/base";

Vue.use(Router);

const createRoutes = (routes) =>
  routes.map((route) => ({
    props: true,
    ...route,
  }));

const routerConfig = {
  mode: "history",
  base: process.env.BASE_URL,
  routes: createRoutes([
    ...collaboration,
    ...institute,
    ...administration,
    ...publications,
    ...user,
    ...base,
  ]),
};

export default new Router(routerConfig);
export const createRouter = () => new Router(routerConfig);
