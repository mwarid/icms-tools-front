// import MoManagement from "@/components/pages/administration/secretariat/MoManagement";
import DueCorrector from "@/components/pages/administration/engagement_office/DueCorrector";
import DueCorrections from "@/components/pages/administration/engagement_office/DueCorrections";
import PhdProjectList from "@/components/pages/administration/mo/PhdProjectList";

export default [
  // Temporarily disable MoManagement until the page is finished
  // {
  //   path: "/administration/secretariat/mo-management/:year?",
  //   name: "mo-management",
  //   component: MoManagement,
  // },
  {
    path: "/administration/engagement-office/due-corrector",
    alias: ["/epr/correctDue"],
    name: "due-corrector",
    component: DueCorrector,
  },
  {
    path: "/administration/engagement-office/due-corrections",
    alias: ["/epr/dueCorrections"],
    name: "due-corrections",
    component: DueCorrections,
  },
  {
    path: "/administration/mo/mo-project-list",
    name: "mo-project-list",
    component: PhdProjectList,
    props: (route) => ({
      year: Number(route.query.year) || new Date().getFullYear(),
    }),
  }
];
