import Dashboard from "@/components/pages/Dashboard";
import ComingSoon from "@/components/pages/ComingSoon";
import App404 from "@/components/pages/App404";
import Navigation from "@/components/pages/Navigation";

export default [
  {
    path: "/collaboration",
    name: "collaboration",
    component: Navigation,
    props: {
      section: "collaboration",
    },
  },
  {
    path: "/institute",
    name: "institute",
    component: Navigation,
    props: {
      section: "institute",
    },
  },
  {
    path: "/administration",
    name: "administration",
    component: Navigation,
    props: {
      section: "administration",
    },
  },
  {
    path: "/publications",
    name: "publications",
    component: Navigation,
    props: {
      section: "publications",
    },
  },
  {
    path: "/coming-soon",
    name: "coming-soon",
    component: ComingSoon,
  },
  {
    path: "/",
    name: "dashboard",
    component: Dashboard,
  },
  {
    path: "*",
    name: "404",
    component: App404,
  },
];
