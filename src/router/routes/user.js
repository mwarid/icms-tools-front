import UserProfile from "@/components/pages/user/UserProfile";
import ALMentions from "@/components/pages/user/ALMentions";
import EmailList from "@/components/pages/user/admin/EmailList";
import ComposeEmail from "@/components/pages/user/admin/ComposeEmail";
import AdminAnnouncements from "@/components/pages/user/admin/AdminAnnouncements";
import Permissions from "@/components/pages/user/admin/Permissions";
import MarkdownEditor from "@/components/pages/user/admin/MarkdownEditor";
import TextHistoryPage from "@/components/pages/user/admin/TextHistoryPage";
import ApplicantDues from "@/components/pages/user/admin/ApplicantDues";
import CBIFlags from "@/components/pages/user/admin/CBIFlags";
import FreeAuthorshipMO from "@/components/pages/user/admin/FreeAuthorshipMO";
import DateEndSign from "@/components/pages/user/admin/DateEndSign";
import AwardTypes from "@/components/pages/collaboration/awards/AwardTypes";

export default [
  {
    path: "/user/profile/:cmsId?",
    alias: ["/users/profile/:cmsId?"],
    name: "user-profile",
    component: UserProfile,
  },
  {
    path: "/user/al-mentions",
    alias: ["/users/signedPapers"],
    name: "al-mentions",
    component: ALMentions,
  },
  {
    path: "/admin/emails",
    name: "email-list",
    component: EmailList,
  },
  {
    path: "/admin/compose-email",
    alias: ["/admin/send_email"],
    name: "compose-email",
    component: ComposeEmail,
  },
  {
    path: "/admin/announcements",
    name: "announcements",
    component: AdminAnnouncements,
  },
  {
    path: "/admin/permissions",
    name: "permissions",
    component: Permissions,
  },
  {
    path: "/admin/markdown-editor",
    alias: ["/admin/markdown"],
    name: "markdown-editor",
    component: MarkdownEditor,
  },
  {
    path: "/admin/text-history",
    name: "text-history",
    component: TextHistoryPage,
  },
  {
    path: "/admin/applicant-dues",
    name: "applicant-dues",
    component: ApplicantDues,
  },
  {
    path: "/admin/cbi-flags",
    name: "cbi-flags",
    component: CBIFlags,
  },
  {
    path: "/admin/free-authorship-mo",
    name: "free-authorship-mo",
    component: FreeAuthorshipMO,
  },
  {
    path: "/admin/date-end-sign",
    name: "date-end-sign",
    component: DateEndSign,
  },
  {
    path: "/admin/award-types",
    name: "award-types",
    component: AwardTypes,
  },
];
