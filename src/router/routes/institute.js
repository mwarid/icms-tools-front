import InstituteProfile from "@/components/pages/institute/InstituteProfile";
import PendingAuthors from "@/components/pages/institute/PendingAuthors";
import MemberAuthors from "@/components/pages/institute/MemberAuthors";
import VoteDelegation from "@/components/pages/institute/VoteDelegation";
import Suspensions from "@/components/pages/institute/Suspensions";
import OverdueGraduations from "@/components/pages/institute/OverdueGraduations";

export default [
  {
    path: "/institute/overdue-graduations",
    alias: ["/inst/overdueGraduations", "/data/overdueGraduations"],
    name: "overdue-graduations",
    component: OverdueGraduations,
  },
  {
    path: "/institute/suspensions",
    alias: ["/inst/suspensions"],
    name: "suspensions",
    component: Suspensions,
  },
  {
    path: "/institute/profile/:urlInstCode?",
    alias: ["/inst/:urlInstCode?", "/inst/members/:instCode?"],
    name: "institute-profile",
    component: InstituteProfile,
  },
  {
    path: "/institute/pending-authors",
    alias: ["/al/pendingAuthors"],
    name: "pending-authors",
    component: PendingAuthors,
  },
  {
    path: "/institute/member-authors",
    alias: ["/al/memberAuthors"],
    name: "member-authors",
    component: MemberAuthors,
  },
  {
    path: "/institute/vote-delegation",
    name: "vote-delegation",
    alias: ["/voting/delegate"],
    component: VoteDelegation,
  },
];
