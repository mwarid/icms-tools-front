import CadiLines from "@/components/pages/publications/cadi/CadiLines";
import ArcMembers from "@/components/pages/publications/cadi/ArcMembers";
import Awgs from "@/components/pages/publications/cadi/Awgs";
import PubCommDashboard from "@/components/pages/publications/cadi/PubCommDashboard";
import CheckLatexBuild from "@/components/pages/publications/cadi/CheckLatexBuild";
import OpenAuthorLists from "@/components/pages/publications/author_lists/OpenAuthorLists";
import AuthorListsFiles from "@/components/pages/publications/author_lists/AuthorListsFiles";
import AuthorDataManagement from "@/components/pages/publications/author_lists/AuthorDataManagement";
import NoteEntries from "@/components/pages/publications/notes/NoteEntries";
import NoteStubs from "@/components/pages/publications/notes/NoteStubs";
import MyNotes from "@/components/pages/publications/notes/MyNotes";
import NotesSecretariat from "@/components/pages/publications/notes/NotesSecretariat";

export default [
  {
    path: "/publications/cadi/lines",
    alias: ["/cadi/lines"],
    name: "cadi-lines",
    component: CadiLines,
    props: { query: {} },
  },
  {
    path: "/publications/cadi/lines/approved",
    alias: ["/cadi/approvedCadiLines"],
    name: "cadi-lines-approved",
    component: CadiLines,
    props: { query: { activityType: "PHYS-APP" } },
  },
  {
    path: "/publications/cadi/lines/updated",
    alias: ["/cadi/updatedCadiLines"],
    name: "cadi-lines-updated",
    component: CadiLines,
    props: { query: { activityType: "modification" } },
  },
  {
    path: "/publications/cadi/arc-members",
    alias: ["/cadi/arcs/members"],
    name: "arc-members",
    component: ArcMembers,
  },
  {
    path: "/publications/cadi/awgs",
    alias: ["/cadi/awgs"],
    name: "awgs",
    component: Awgs,
  },
  {
    path: "/publications/cadi/check-latex-build",
    alias: ["/cadi/checkLatexBuild"],
    name: "check-latex-build",
    component: CheckLatexBuild,
  },
  {
    path: "/publications/author-lists/open-author-lists",
    alias: ["/al/openLists"],
    name: "open-author-lists",
    component: OpenAuthorLists,
  },
  {
    path: "/publications/author-lists/files",
    name: "author-lists-files",
    component: AuthorListsFiles,
  },
  {
    path: "/publications/author-lists/author-data",
    name: "author-data-management",
    component: AuthorDataManagement,
  },
  {
    path: "/publications/notes/entries/:noteType?/:year?/:index?",
    alias: ["/notes/entries/:noteType?/:year?/:index?"],
    name: "notes",
    component: NoteEntries,
    props: (route) => {
      return {
        noteType: route.params.noteType,
        year: Number(route.params.year)
          ? Number(route.params.year)
          : new Date().getFullYear(),
        index: route.params.index,
      };
    },
  },
  {
    path: "/publications/notes/pending/:noteType?/:year?",
    alias: ["/notes/pending/:noteType?/:year?"],
    name: "note-stubs",
    component: NoteStubs,
    props: (route) => {
      return {
        noteType: route.params.noteType,
        year: Number(route.params.year) ? Number(route.params.year) : undefined,
      };
    },
  },
  {
    path: "/publications/notes/mine",
    alias: ["/notes/mine"],
    name: "notes-mine",
    component: MyNotes,
  },
  {
    path: "/publications/notes/secretariat",
    alias: ["/notes/secretariat"],
    name: "notes-secretariat",
    component: NotesSecretariat,
  },
  {
    path: "/publications/cadi/dashboard",
    alias: ["/cadi/dashboard"],
    name: "pub-comm-dashboard",
    component: PubCommDashboard,
  },
];
