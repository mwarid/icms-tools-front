import Vue from "vue";
import Vuex from "vuex";

import user from "./modules/user";
import { notificationModule } from "common";

Vue.use(Vuex);

const storeConfig = {
  modules: {
    user,
    notification: notificationModule,
  },
};

export default new Vuex.Store(storeConfig);
export const createStore = () => new Vuex.Store(storeConfig);
