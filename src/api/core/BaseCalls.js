import { BaseCall } from "common";
import { restplusApi } from "@/api/core/axiosWrapper";
/**
 * TODOs:
 * - var data = $.param({ foo: ['bar', 'baz'] }, true); - when sending GET request with multiple values for the same parameter
 * - conditionally trim nulls before submitting POSTs
 */
export class RestplusCall extends BaseCall {
  constructor(endpoint) {
    super(restplusApi, endpoint);
  }
}

/**
 * A base class for the calls following the convention imposed by the AbstractCRUDApiUnit on the server side.
 * Under the hood it temporarily changes the endpoint's address so that the underlying methods still work.
 */
export class CrudCall extends RestplusCall {
  constructor(endpoint) {
    super(endpoint);
    this.initialEndpoint = this.endpoint;
  }

  /**
   * Fetches the instance specified by @param resourceId
   */
  getOne(resourceId) {
    this.endpoint = this.initialEndpoint + "/" + resourceId;
    let retval = this.get();
    this.endpoint = this.initialEndpoint;
    return retval;
  }

  /**
   * Updates the instance specified by @param resourceId
   */
  putOne(resourceId) {
    this.endpoint = this.initialEndpoint + "/" + resourceId;
    let retval = this.put();
    this.endpoint = this.initialEndpoint;
    return retval;
  }

  /**
   * Deletes the instance specified by @param resourceId
   */
  deleteOne(resourceId) {
    this.endpoint = this.initialEndpoint + "/" + resourceId;
    let retval = this.delete();
    this.endpoint = this.initialEndpoint;
    return retval;
  }
}
