import { createAxiosInstance } from "common";
import store from "@/store";

export const restplusApi = createAxiosInstance({
  baseURL: process.env.VUE_APP_API_URL + "/restplus",
  config: {
    headers:
      process.env.NODE_ENV === "development"
        ? {
            "X-Auth-Username": process.env.VUE_APP_USERNAME,
            User: process.env.VUE_APP_USER,
          }
        : {},

    paramsSerializer: function (params) {
      let urlParams = new URLSearchParams();
      Object.entries(params).forEach((e) => {
        if (Array.isArray(e[1])) {
          e[1].forEach((v) => urlParams.append(e[0], v));
        } else {
          urlParams.append(e[0], e[1]);
        }
      });
      return urlParams.toString();
    },
  },
  defaultErrorHandler: (err) => {
    store.dispatch(
      "notification/errorMessage",
      "AJAX error " + err.response.status + ": " + err.response.data.message
    );
  },
});

export const restplusNamespace = (namespace) =>
  ["get", "post", "put", "delete"].reduce(
    (acc, method) => ({
      ...acc,
      [method]: (url, ...rest) => restplusApi[method](namespace + url, ...rest),
    }),
    {}
  );
