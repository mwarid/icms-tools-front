import { RestplusCall } from "@/api/core";

export class AwardsCall extends RestplusCall {
  constructor() {
    super("/awards");
  }

  setAwardTypeId(value) {
    return this.set("award_type_id", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
  setNominationsOpenDate(value) {
    return this.set("nominations_open_date", value);
  }
  setNominationsDeadline(value) {
    return this.set("nominations_deadline", value);
  }
  setAwardeePublicationDate(value) {
    value ||= null;
    return this.set("awardee_publication_date", value);
  }
  fetchOnlyAcceptingNominations() {
    return this.set("only_accepting_nominations", true);
  }
}

export class AwardTypesCall extends RestplusCall {
  constructor() {
    super("/awards/types");
  }

  setType(value) {
    return this.set("type", value);
  }
}

export class AwardeesCall extends RestplusCall {
  constructor() {
    super("/awards/awardees");
  }

  setAwardId(value) {
    return this.set("award_id", value);
  }
}

export class AwardNomineesCall extends RestplusCall {
  constructor() {
    super("/awards/nominees");
  }

  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setName(value) {
    return this.set("name", value);
  }
  setHrId(value) {
    return this.set("hr_id", value);
  }
  setOrganization(value) {
    return this.set("organization", value);
  }
  setSubsystem(value) {
    return this.set("subsystem", value);
  }
}

export class AwardNominationsCall extends RestplusCall {
  constructor() {
    super("/awards/nominations");
  }

  setIncludeDeleted(value) {
    return this.set("include_deleted", value);
  }
  fetchOwnNominationsOnly() {
    return this.set("own_nominations_only", true);
  }
  setNomineeCmsId(value) {
    return this.set("nominee_cms_id", value);
  }
  setAwardId(value) {
    return this.set("award_id", value);
  }
  setRemarks(value) {
    value ||= null;
    return this.set("remarks", value);
  }
  setProposedCitation(value) {
    value ||= null;
    return this.set("proposed_citation", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
  setNominationProjects(project_array) {
    return this.set("nomination_projects", project_array.sort().join(";"));
  }
  setWorkingRelationship(value) {
    return this.set("working_relationship", value);
  }
  setJustification(value) {
    return this.set("justification", value);
  }
}

export class AwardsProjectsCall extends RestplusCall {
  constructor() {
    super("/awards/projects");
  }
}
