import { RestplusCall } from "@/api/core";

export class AppAssetDataCall extends RestplusCall {
  constructor() {
    super("/diagnostics/app_asset");
  }
  setAssetName(value) {
    return this.set("asset_name", value);
  }
}

export class DeploymentInfoCall extends RestplusCall {
  constructor() {
    super("/diagnostics/deployment_info");
  }
}
