import { RestplusCall } from "@/api/core";

export class MoListCall extends RestplusCall {
  constructor() {
    super("/mo/list");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
  setFaId(value) {
    return this.set("fa_id", value);
  }
  setInstCode(value) {
    return this.set("inst_code", value);
  }
}

export class MoStepsCall extends RestplusCall {
  constructor() {
    super("/mo/steps");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
}

export class PersonMoInfoCall extends RestplusCall {
  constructor() {
    super("/mo/person_mo");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class MoPeopleListCall extends RestplusCall {
  constructor() {
    super("/mo/people_list");
  }
  setYear(value) {
    return this.set("year", value);
  }
  setMode(value) {
    return this.set("mode", value);
  }
}

export class PhdProjectListCall extends RestplusCall {
  constructor() {
    super("/mo/phd_project_list");
  }
  setYear(value) {
    return this.set("mo_year", value);
  }
}
