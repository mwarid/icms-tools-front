import { RestplusCall } from "@/api/core";

export class ImagesListCall extends RestplusCall {
  constructor() {
    super("/general/images");
  }
  setId(value) {
    return this.set("id", value);
  }
  setName(value) {
    return this.set("name", value);
  }
}

export class MarkdownListCall extends RestplusCall {
  constructor() {
    super("/general/markdown_list");
  }
  setId(value) {
    return this.set("id", value);
  }
  setName(value) {
    return this.set("name", value);
  }
}

export class MarkdownCall extends RestplusCall {
  constructor() {
    super("/general/markdown");
  }
  setId(value) {
    return this.set("id", value);
  }
  setName(value) {
    return this.set("name", value);
  }
  setBody(value) {
    return this.set("body", value);
  }
}
