import { RestplusCall } from "@/api/core";

export class ProjectsCall extends RestplusCall {
  constructor() {
    super("/epr/projects");
  }
  setCode(value) {
    return this.set("code", value);
  }
}

export class EprCorrectionCall extends RestplusCall {
  constructor(type) {
    super(type == "author" ? "/epr/waive_author_due" : "/epr/rectify_app_due");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
  setCorrection(value) {
    return this.set("correction", value);
  }
  setInstCode(value = null) {
    return this.set("inst_code", value);
  }
  setRemarks(value = null) {
    return this.set("remarks", value);
  }
  setReason(value = null) {
    return this.set("reason", value);
  }
}

export class EprTimeLinesCall extends RestplusCall {
  constructor() {
    super("/epr/timeline");
  }
  setYear(value) {
    return this.set("year", value);
  }
  setInstCode(value = null) {
    return this.set("inst_code", value);
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class EprCorrectionsCall extends RestplusCall {
  constructor() {
    super("/epr/due_corrections");
  }
}

export class EprInstStatsCall extends RestplusCall {
  constructor() {
    super("/epr/inst/stats");
  }
  setCode(value) {
    return this.set("code", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
}
