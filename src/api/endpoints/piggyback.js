import {
  IcmsPiggybackCall,
  IcmsPiggybackHalCall,
  restplusApi,
} from "@/api/core";

export class PeopleCall extends IcmsPiggybackHalCall {
  constructor() {
    super("/people");
  }
  setCmsIds(values) {
    return this.set("cmsIds", values.join(","));
  }
}

export class ArcCountsCall extends IcmsPiggybackHalCall {
  constructor() {
    super("/cadi/history/arcsPerYear");
  }
  setInstCode(value) {
    return this.set("instCode", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
}

export class InstAddressesCall extends IcmsPiggybackCall {
  constructor(instCode) {
    var path = "/institutes/addresses";
    if (instCode !== null && instCode != undefined) {
      path += "/" + instCode;
    }
    super(path);
  }
}

export class EmeritusApprovalsCall extends IcmsPiggybackCall {
  constructor() {
    super("/people/statuses/requests");
    this.set("concern", "CMSEMERITUS");
  }
  setYear(value) {
    return this.set("year", value);
  }
}

export class TempFileUploadCall extends IcmsPiggybackCall {
  constructor() {
    super("/tempFiles");
    this.data = new FormData();
    this.processData = false;
    this.contentType = false;
    this.dataType = null;
  }
  set(key, value) {
    this.data.append(key, value);
    return this;
  }
  setFile(value) {
    return this.set("file", value);
  }

  initAjaxCall() {
    return restplusApi.upload("/relay/piggyback/tempFiles", this.data);
  }
}
