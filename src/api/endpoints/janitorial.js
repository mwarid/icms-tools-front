import { RestplusCall } from "@/api/core";

export class HistoryLinesCall extends RestplusCall {
  constructor() {
    super("/janitorial/text_history");
  }
}

export class AccountStatusCheckCall extends RestplusCall {
  constructor() {
    super("/janitorial/account_status");
    return this.set("action", "check");
  }
  setHrId(value) {
    return this.set("hr_id", value);
  }
}

export class TextHistoryCall extends RestplusCall {
  constructor() {
    super("/janitorial/text_history");
  }

  setHistory(value) {
    return this.set("history", value);
  }
}
