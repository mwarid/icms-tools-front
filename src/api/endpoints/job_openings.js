const fakeCall = (resp) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(resp);
    }, 300);
  });

export const getJobOpenings = () =>
  fakeCall([
    {
      id: 1,
      title: "Spokesperson",
      description: "Job as Spokesperson - not as easy as it looks ...",
      requirements: "Excellent management and communication skills",
      start_date: "01-09-2021",
      end_date: "31-08-2023",
      deadline: "09-07-2021",
      position_id: 1,
      position: "Spokesperson",
      status: "created",
      last_modified: "01-01-2017",
    },
    {
      id: 2,
      title: "Spokesperson",
      description: "Job as Spokesperson - not as easy as it looks ...",
      requirements: "Excellent management and communication skills",
      start_date: "01-09-2021",
      end_date: "31-08-2023",
      deadline: "09-07-2021",
      position_id: 1,
      position: "Spokesperson",
      status: "created",
      last_modified: "01-01-2017",
    },
    {
      id: 3,
      title: "Spokesperson",
      description: "Job as Spokesperson - not as easy as it looks ...",
      requirements: "Excellent management and communication skills",
      start_date: "01-09-2021",
      end_date: "31-08-2023",
      deadline: "09-07-2021",
      position_id: 1,
      position: "Spokesperson",
      status: "created",
      last_modified: "01-01-2017",
    },
  ]);

export const putJobOpenings = (job) => fakeCall(job);
export const postJobOpenings = (job) => fakeCall({ id: 10, ...job });
