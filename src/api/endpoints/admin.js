import { RestplusCall, CrudCall } from "@/api/core/BaseCalls";

export class EmailsCall extends RestplusCall {
  constructor() {
    super("/admin/emails");
  }
}

export class EmailCall extends RestplusCall {
  constructor() {
    super("/admin/email");
  }
  setId(value) {
    return this.set("id", value);
  }
  setSender(value) {
    return this.set("sender", value);
  }
  setTo(value) {
    return this.set("to", value);
  }
  setBcc(value) {
    return this.set("bcc", value);
  }
  setCc(value) {
    return this.set("cc", value);
  }
  setSubject(value) {
    return this.set("subject", value);
  }
  setBody(value) {
    return this.set("body", value);
  }
}

export class AnnouncementsCall extends RestplusCall {
  constructor() {
    super("/admin/announcements");
  }
}

export class AnnouncementCall extends RestplusCall {
  constructor() {
    super("/admin/announcement");
  }
  setType(value) {
    return this.set("type", value);
  }
  setSubject(value) {
    return this.set("subject", value);
  }
  setContent(value) {
    return this.set("content", value);
  }
  setStartDatetime(value) {
    return this.set("start_datetime", value);
  }
  setEndDatetime(value) {
    return this.set("end_datetime", value);
  }
  setApplication(value) {
    return this.set("application", value);
  }
}

export class AuthorizationsCall extends RestplusCall {
  constructor() {
    super("/admin/permissions/rights");
  }
  setResourceId(value) {
    return this.set("resource_id", value);
  }
  setAccessClassId(value) {
    return this.set("access_class_id", value);
  }
  setAction(value) {
    return this.set("action", value);
  }
}

export class ResourcesCall extends RestplusCall {
  constructor() {
    super("/admin/permissions/resources");
  }
  setFilters(value) {
    return this.set("filters", value);
  }
  setKey(value) {
    return this.set("key", value);
  }
  setType(value) {
    return this.set("type", value);
  }
}

export class AccessClassesCall extends CrudCall {
  constructor() {
    super("/admin/permissions/classes");
  }
  setName(value) {
    return this.set("name", value);
  }
  setRules(value) {
    return this.set("rules", value);
  }
}

export class RoutesCall extends RestplusCall {
  constructor() {
    super("/admin/routes");
  }
}

export class ParamStoresCall extends RestplusCall {
  constructor() {
    super("/admin/permissions/stores");
  }
}

export class ImpersonateUserCall extends RestplusCall {
  constructor() {
    super("/admin/impersonate");
  }
}

export class CBIFlagsCall extends RestplusCall {
  constructor() {
    super("/admin/cbi-flags");
  }
}

export class FreeAuthorshipMOCall extends RestplusCall {
  constructor() {
    super("/admin/free-authorship-mo");
  }
}

export class DateEndSignCall extends RestplusCall {
  constructor() {
    super("/admin/date-end-sign");
  }
}

export class ApplicantDuesCall extends RestplusCall {
  constructor() {
    super("/admin/applicant-dues");
  }
}
