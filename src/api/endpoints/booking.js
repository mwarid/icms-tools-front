import { RestplusCall } from "@/api/core";

export class BookingWeeksCall extends RestplusCall {
  constructor() {
    super("/booking/weeks");
  }
  setTitle(value) {
    return this.set("title", value);
  }
  setDate(value) {
    return this.set("date", value);
  }
  setDateEnd(value) {
    return this.set("date_end", value);
  }
  setIsExternal(value) {
    return this.set("is_external", value);
  }
}

export class BookingRoomsCall extends RestplusCall {
  constructor() {
    super("/booking/rooms");
  }
  setWeekId(value) {
    return this.set("weekId", value);
  }

  setIndicoId(value) {
    return this.set("indico_id", value);
  }
  setBuilding(value) {
    return this.set("building", value);
  }
  setFloor(value) {
    return this.set("floor", value);
  }
  setRoomNr(value) {
    return this.set("room_nr", value);
  }
  setCustomName(value) {
    return this.set("custom_name", value);
  }
  setAtCern(value) {
    return this.set("at_cern", value);
  }
}

export class BookingSlotsCall extends RestplusCall {
  constructor() {
    super("/booking/slots");
  }
  setWeekId(value) {
    return this.set("weekId", value);
  }
}

export class BookingRequestsCall extends RestplusCall {
  constructor() {
    super("/booking/requests");
  }
  setWeekId(value) {
    return this.set("cms_week_id", value);
  }
  setCmsWeekId(value) {
    return this.set("cms_week_id", value);
  }
  setPreferredRoomId(value) {
    return this.set("room_id_preferred", value);
  }
  setCmsRoomId(value) {
    return this.set("room_id", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
  setCmsIdFor(value) {
    return this.set("cms_id_for", value);
  }
  setTitle(value) {
    return this.set("title", value);
  }
  setProject(value) {
    return this.set("project", value);
  }
  setWebcast(value) {
    return this.set("webcast", value);
  }
  setOfficial(value) {
    return this.set("official", value);
  }
  setTimeStart(value) {
    return this.set("time_start", value);
  }
  setDuration(value) {
    return this.set("duration", value);
  }
  setCapacity(value) {
    return this.set("capacity", value);
  }
  setPassword(value) {
    return this.set("password", value);
  }
  setAgendaUrl(value) {
    return this.set("agenda_url", value);
  }
  setRemarks(value) {
    return this.set("remarks", value);
  }
  setReason(value) {
    return this.set("reason", value);
  }
  setConveners(value) {
    return this.set("conveners", value);
  }
}

export class BookingStatusesCall extends RestplusCall {
  constructor() {
    super("/booking/requests/statuses");
  }
  setStatus(value) {
    return this.set("status", value);
  }
}

export class BookingProjectsCall extends RestplusCall {
  constructor() {
    super("/booking/projects");
  }
}
