import { RestplusCall } from "@/api/core";

export class LegacyNotesSetSubmitter extends RestplusCall {
  constructor() {
    super("/old_notes/submitter");
  }
  setCmsNoteId(value) {
    return this.set("cms_note_id", value);
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class CMSNotesSuggestionsCall extends RestplusCall {
  constructor(searchTerm) {
    super("/old_notes/autocomplete_node_ids");
    this.set("search_term", searchTerm);
    this.set("exclude_frozen_deleted", true);
  }
}
