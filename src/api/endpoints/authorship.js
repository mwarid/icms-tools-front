import { RestplusCall } from "@/api/core";

export class ApplicationsInfoCall extends RestplusCall {
  constructor() {
    super("/authorship/app_checks");
  }
  setLastRunOnly(value) {
    return this.set("last_run_only", value);
  }
}

export class AuthorStatsCall extends RestplusCall {
  constructor() {
    super("/authorship/author_stats");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class CheckRightsCall extends RestplusCall {
  constructor() {
    super("/authorship/check_rights");
  }
}

export class AuthorDataCall extends RestplusCall {
  constructor() {
    super("/authorship/authordata");
  }

  setInspireId(value) {
    return this.set("inspire_id", value);
  }
  setLastUpdate(value) {
    return this.set("last_update", value);
  }
  setOrcId(value) {
    return this.set("orc_id", value);
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}
