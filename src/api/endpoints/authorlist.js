import { RestplusCall } from "@/api/core";

export class MemberAuthorsCall extends RestplusCall {
  constructor() {
    super("/authorlists/memberAuthors");
  }
  setCmsId(value) {
    return this.set("cmsId", value);
  }
  setFlagId(value) {
    return this.set("flagId", value);
  }
}

export class PendingAuthorsCall extends RestplusCall {
  constructor() {
    super("/authorlists/pendingAuthors");
  }
  setCmsId(value) {
    return this.set("cmsId", value);
  }
  setAction(value) {
    return this.set("action", value);
  }
}

export class AuthorListsInfoCall extends RestplusCall {
  constructor() {
    super("/authorlists/al_info");
  }
  setCode(value) {
    return this.set("code", value);
  }
  setStatus(value) {
    return this.set("al_status", value);
  }
}

export class AuthorListMentionsCall extends RestplusCall {
  constructor() {
    super("/authorlists/al_mentions");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class AuthorListListsCall extends RestplusCall {
  constructor() {
    super("/authorlists/lists");
  }
  setPaperCode(value) {
    return this.set("paper_code", value);
  }

  setForceRewrite(value) {
    return this.set("force_rewrite", value);
  }
}

export class AuthorListFilesetsCall extends RestplusCall {
  constructor() {
    super("/authorlists/filesets");
  }
  setPaperCode(value) {
    return this.set("paper_code", value);
  }

  setForceRewrite(value) {
    return this.set("force_rewrite", value);
  }
}

export class AuthorListFilesCall extends RestplusCall {
  constructor() {
    super("/authorlists/files");
  }
  setPaperCode(value) {
    return this.set("paper_code", value);
  }
}
