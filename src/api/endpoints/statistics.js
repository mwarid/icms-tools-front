import { RestplusCall, restplusNamespace } from "@/api/core";
import { trimJson, expandParametersIntoUrl } from "@/utils/helpers";

const restplus = restplusNamespace("/statistics");

export const getMembersCount = ({
  groupBy = ["nationality"],
  cmsStatuses = ["CMS"],
  authorFlags = [],
  yearFrom,
  yearTo
}) => {
  var url = "/members_count?";
  authorFlags.forEach((e) => (url += "is_author=" + e + "&"));
  cmsStatuses.forEach((e) => (url += "status=" + e + "&"));
  groupBy.forEach((e) => (url += "grouping_column=" + e + "&"));
  url = url.substring(0, url.length - 1);
  url += "&" + "from_year=" + yearFrom + "&" + "to_year=" + yearTo
  return restplus.get(url);
};

export class MembersCountCall extends RestplusCall {
  constructor() {
    super("/statistics/members_count");
    this.set("status", []);
  }
  activeMembers() {
    this.data["status"].push("CMS");
    return this;
  }
  groupByInst() {
    return this.set("grouping_column", "inst_code");
  }
  onlyAuthors() {
    return this.set("is_author", true);
  }
  fromYear(year) {
    return this.set("from_year", year)
  }
  toYear(year) {
    return this.set("to_year", year)
  }
}

export class EprAggregatesCall extends RestplusCall {
  constructor({
    cmsId,
    instCode,
    projectCode,
    year,
    aggUnits = ["pledges done", "shifts done"],
    groupBy = ["year", "cms_id"],
  }) {
    super(
      expandParametersIntoUrl(
        "/statistics/epr/aggregates?",
        trimJson({
          year: year,
          cms_id: cmsId,
          inst_code: instCode,
          project_code: projectCode,
          agregate_unit: aggUnits,
          grouping_columns: groupBy,
        })
      )
    );
  }
}
