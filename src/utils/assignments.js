export default class AssignmentLinker {
  static linkAssignmentToPerson(assignment, person) {
    const projectInformation = {
      code: assignment.project_code,
      fraction: assignment.fraction,
      id: assignment.id,
    };
    person.assignments.push(projectInformation);
  }

  static setDefaultAssignments(people) {
    for (let person of people) {
      if (!person.assignments.length && person.project) {
        person.assignments = [
          {
            code: person.project,
            fraction: 100,
            id: 0,
          },
        ];
      }
    }
  }

  static linkAssignmentsToPeople(assignments, people) {
    let assignmentsOfPeople = new Map();

    for (let assignment of assignments) {
      let assignmentOfPerson = assignmentsOfPeople.get(assignment.cms_id);
      if (assignmentOfPerson) {
        assignmentOfPerson.push(assignment);
      } else {
        assignmentOfPerson = [assignment];
      }
      assignmentsOfPeople.set(assignment.cms_id, assignmentOfPerson);
    }
    for (let person of people) {
      assignments = assignmentsOfPeople.get(person.cmsId) || [];
      for (let assignment of assignments) {
        this.linkAssignmentToPerson(assignment, person);
      }
    }
    this.setDefaultAssignments(people);
  }
}
