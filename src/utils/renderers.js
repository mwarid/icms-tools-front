export default {
  assignments: value => {
    let cell = ""
    if (value.length > 1) {
      for (let assignment of value)
        cell += `${cell ? ", " : ""}${assignment.code}: ${
          100 * assignment.fraction
        }%`
    } else if (value.length === 1) cell = value[0].code
    return cell
  }
}