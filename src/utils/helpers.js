export function uniqueBy(array, key) {
  var index = [];
  return array.filter(function (item) {
    var k = key(item);
    return index.indexOf(k) >= 0 ? false : index.push(k);
  });
}

export function sortAlphabeticallyBy(array, key) {
  return array.sort((a, b) => {
    const aKey = key(a);
    const bKey = key(b);
    if (aKey < bKey) {
      return -1;
    }
    if (aKey > bKey) {
      return 1;
    }
    return 0;
  });
}

export function debounce(fn, delay) {
  let timeoutID = null;
  return function () {
    clearTimeout(timeoutID);
    let args = arguments;
    let that = this;
    timeoutID = setTimeout(function () {
      fn.apply(that, args);
    }, delay);
  };
}

// We need to get rid of those 2 once all api calls are properly converted
export function trimJson(data) {
  for (var key in data) {
    if (data[key] === null) {
      delete data[key];
    }
  }
  return data;
}

export function expandParametersIntoUrl(endpoint, params) {
  endpoint += endpoint.endsWith("?") ? "" : "?";
  Object.entries(params).forEach(([key, value]) => {
    if (!endpoint.endsWith("?") && !endpoint.endsWith("&")) {
      endpoint += "&";
    }
    if (Array.isArray(value)) {
      value.forEach((e, idx, arr) => {
        endpoint += key + "=" + e + (idx < arr.length - 1 ? "&" : "");
      });
    } else {
      endpoint += key + "=" + value;
    }
  });
  return endpoint;
}

export class Debouncer {
  /**
   *
   * @param {Function} callback
   * @param {Number} timeout a negative value will defer countdown's onset until after reset is called
   */
  constructor(callback, timeout = 100) {
    this.callback = callback;
    this.timeoutMs = timeout;
    this.timeoutId = null;
    if (timeout > 0) {
      this.reset();
    }
  }
  reset(newTimeout = undefined) {
    if (Number(newTimeout)) {
      this.timeoutMs = Number(newTimeout);
    }
    if (this.timeoutId !== null) {
      clearTimeout(this.timeoutId);
    }
    this.timeoutId = setTimeout(this.callback, this.timeoutMs);
  }
}

export function preformatObject(object, indent = 32, indentChar = ".") {
  return Object.entries(object)
    .map(
      (e) =>
        e[0].padEnd(indent, indentChar) +
        ": " +
        String(e[1])
          .match(/.{0,64}/g)
          .reduce(
            (a, b) =>
              a + (a.length * b.length > 0 ? "\n".padEnd(indent + 3) : "") + b,
            ""
          ) +
        "\n"
    )
    .reduce((a, b) => a + b, "");
}

export function deepCopyArray(array) {
  let copy = [];
  for (let object of array) {
    copy.push(JSON.parse(JSON.stringify(object)));
  }
  return copy;
}
