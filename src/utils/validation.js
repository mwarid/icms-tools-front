import { extend } from "vee-validate";
import * as rules from "vee-validate/dist/rules";

export const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

// Field validation rules
export const fieldRequiredRule = (fieldName) => (value) =>
  (!!value && !(Array.isArray(value) && value.length === 0)) ||
  `${fieldName} is required!`;
export const emailAddressRule = (fieldName) => (value) =>
  validateEmail(value) || `${fieldName} must be an email address!`;
export const emailAddressListRule = (fieldName) => (value) => {
  const emails = value.split(/[ ,]+/);
  for (const email of emails) {
    if (email && !validateEmail(email)) {
      return `${fieldName} must only include valid email addresses!`;
    }
  }
  return true;
};
export const charLimitRule = (charLimit) => (value) =>
  !value ||
  value.length <= charLimit ||
  `Character limit exceeded: ${value.length} / ${charLimit} characters used!`;

// TODO : Validate date
export const dateFieldRule = (fieldName) => (value) =>
  Date.parse(value) || !value || `${fieldName} must be a date!`;

// Vee-Validate rules
export const applyRules = () => {
  Object.keys(rules).forEach((rule) => {
    extend(rule, rules[rule]);
  });
};
