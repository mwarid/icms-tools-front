import Vue from "vue";
import store from "@/store";
import router from "@/router";

import plugin from "common";
import App from "@/App.vue";

import { ValidationProvider, ValidationObserver } from "vee-validate";
import { applyRules } from "@/utils/validation";

Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);
applyRules();

const options = {
  store,
  router,
  render: (h) => h(App),
};
Vue.use(plugin, options);

// TODO : configure this with process.env
Vue.config.productionTip = false;

new Vue(options).$mount("#app");
