default:
  image: node:16

  # Validate that the repository contains a package.json and extract a few values from it.
  before_script:
    - |
      if [[ ! -f package.json ]]; then
        echo "No package.json found! A package.json file is required to publish a package to GitLab's NPM registry."
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#creating-a-project'
        exit 1
      fi
    - NPM_PACKAGE_NAME="@${CI_PROJECT_ROOT_NAMESPACE}/$(node -p "require('./package.json').name")"
    - NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
    - |
      if [ -z "$CI_MODE" ]; then
        CI_MODE="production";
        if [[ $CI_COMMIT_BRANCH != "production" && $CI_PROJECT_ROOT_NAMESPACE == "cms-icmsweb" ]]; then
          CI_MODE="stage";
        elif [[ $CI_PROJECT_ROOT_NAMESPACE != "cms-icmsweb" ]]; then
          CI_MODE="playspacms";
        fi
        echo "CI_MODE determined to be ${CI_MODE}"
      else
        echo "CI_MODE pre-set to ${CI_MODE}";
      fi
    - |
      if [ -f ".env.${CI_MODE}" ]; then
        echo "Env file .env.${CI_MODE} already in place";
      else
        echo "Creating .env file: .env.${CI_MODE}";
        echo "BASE_URL=${CI_BASE_URL}" >> .env.${CI_MODE};
        echo "NODE_ENV=${CI_NODE_ENV}" >> .env.${CI_MODE};
        echo "VUE_APP_API_URL=${CI_VUE_APP_API_URL}" >> .env.${CI_MODE};
      fi
    - |
      echo "VUE_APP_COMMIT_BRANCH=${CI_COMMIT_BRANCH}" >> .env.${CI_MODE};
      echo "VUE_APP_COMMIT_TITLE=${CI_COMMIT_TITLE}" >> .env.${CI_MODE};
      echo "VUE_APP_COMMIT_TIMESTAMP=${CI_COMMIT_TIMESTAMP}" >> .env.${CI_MODE};
      echo "VUE_APP_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}" >> .env.${CI_MODE};
      echo "VUE_APP_LAST_COMMIT_HASH=${CI_COMMIT_SHA}" >> .env.${CI_MODE};

variables:
  GIT_SUBMODULE_STRATEGY: recursive

# Validate that the package name is properly scoped to the project's root namespace.
# For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention
validate_package_scope:
  stage: build
  script:
    - |
      if [[ ! $NPM_PACKAGE_NAME =~ ^@$CI_PROJECT_ROOT_NAMESPACE/ ]]; then
        echo "Invalid package scope! Packages must be scoped in the root namespace of the project, e.g. \"@${CI_PROJECT_ROOT_NAMESPACE}/${CI_PROJECT_NAME}\""
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention'
        echo "NPM_PACKAGE_NAME: ${NPM_PACKAGE_NAME}"
        echo "CI_PROJECT_ROOT_NAMESPACE: ${CI_PROJECT_ROOT_NAMESPACE}"
        exit 1
      fi

# If no .npmrc if included in the repo, generate a temporary one to use during the publish step
# that is configured to publish to GitLab's NPM registry
create_npmrc:
  stage: build
  script:
    - |
      if [[ ! -f .npmrc ]]; then
        echo 'No .npmrc found! Creating one now. Please review the following link for more information: https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#authenticating-with-a-ci-job-token'

        {
          echo '@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}:${CI_SERVER_PORT}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/'
          echo '//${CI_SERVER_HOST}:${CI_SERVER_PORT}/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}'
          echo '//${CI_SERVER_HOST}:${CI_SERVER_PORT}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}'
        } >> .npmrc

      fi
  artifacts:
    paths:
      - .npmrc



build_package:
  stage: deploy
  script:
    - cat ".env.${CI_MODE}"
    - npm install
    - npm run build -- --mode $CI_MODE
  artifacts:
    paths:
      - dist